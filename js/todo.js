$(document).ready(function() {

    function todoCookieSet() {
        totalTodoUl = $('#todos').html();
        Cookies.set('todoListCookie', totalTodoUl, {
            expires: 20000
        });
    };


    if (Cookies.get('todoListCookie')) {
        cookieContent = Cookies.get('todoListCookie');
        $("#todos").html(cookieContent);

    } else {
        todoCookieSet();
    }
    // $("#todos").disableSelection();

    function enableSortableWithCookie() {
        $("#todos").sortable({
            stop: function(event, ui) {
                todoCookieSet()
            }
        });
    };
    enableSortableWithCookie();


    $('#addItem').on('click', addItem);
    $('#todos').on('click', '.complete', completeItem)
    $('#todos').on('click', '.glyphicon-trash', deleteItem)
    $('#todos').on('click', '.glyphicon-edit', editItem)
    $('#todos').on('click', '.spanNewTodoText', editItem)
    $('#newTodo').on('keypress', function(event) {

        if (event.which === 13) {
            addItem();
            event.preventDefault();

        };
    });

    function disableRest (thisParent) {
      thisParent.siblings().addClass('disabled').prop('disabled', true);
      $("#todos").siblings().children().addClass('disabled').prop('disabled', true);
      $("#todos").sortable("disable");
    }

    function addItem() {



        var newTodoText = $('#newTodo').val();

        if (newTodoText != "") {
            if ($('#newTodo').hasClass('inputError')) {
                $('#newTodo').removeClass('inputError');
            }


            var spanNewTodoText = $('<span>').text(newTodoText).addClass('col-md-8 col-xs-12');
            spanNewTodoText.addClass('spanNewTodoText');
            var newLi = $('<li>').addClass('list-group-item row')

            addCertainButton(newLi, "glyphicon-unchecked complete col-md-1 col-xs-1", false);
            addCertainButton(newLi, "glyphicon-edit col-md-1 col-xs-1", false);
            addCertainButton(newLi, "glyphicon-trash col-md-1 col-xs-1", true);
            newLi.append(spanNewTodoText);



            $('#todos').prepend(newLi);
            $('#newTodo').val("");

        } else {
            $('#newTodo').attr('placeholder', 'Really nothing to do? Please fill in a new todo!');
            $('#newTodo').addClass('inputError');
        };
        todoCookieSet();

    };

    function addCertainButton(newLi, icon, pullRight) {
        pullRightClass = "";
        if (pullRight == true) {
            pullRightClass = "pull-right";
        } else {
            pullRightClass = "pull-left";
        };

        newSpan = $('<span>').addClass('glyphicon').addClass(icon).addClass(pullRightClass);
        $(newLi).append(newSpan);
        return newLi;
    };


    function completeItem() {
        checkClass = $(this).hasClass('glyphicon-unchecked');
        if (checkClass === true) {
            $(this).parent().addClass('done');
            $(this).removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $(this).parent().removeClass('done');
            $(this).removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
        todoCookieSet();
    }

    function deleteItem() {
        $(this).parent().remove();
        todoCookieSet();
    }

    function editItem() {

        var thisParent = $(this).parent();
        disableRest (thisParent);
        var currentTxt = thisParent.find('.spanNewTodoText').text();

        editInput = $('<input>');
        editInput.attr({
            id: 'editInput',
            class: 'col-xs-10'
        }).val(currentTxt);
        thisParent.append(editInput);
        editInput.siblings().hide();
        editInput.focus().select();
        thisParent.find('.spanNewTodoText').addClass('tempHide');

        addCertainButton(thisParent, "glyphicon-ok-circle col-xs-1", true);

        editInput.on('blur', updateItem);
        $('#todos').on('click', '.glyphicon-ok-circle', updateItem);
        $('#editInput').on('keypress', function(event) {
            if (event.which === 13) {
                updateItem();
                event.preventDefault();

            };
        });

    };

    function updateItem() {
        var thisParent = $('#todos');
        var newText = $('#editInput').val();
        var currentItemLi = $('#editInput').parent();
        currentItemLi.find('.spanNewTodoText').text(newText).removeClass('tempHide');

        thisParent.find('#editInput').siblings().show();
        thisParent.find('#editInput').remove();
        thisParent.find('.glyphicon-ok-circle').remove();

        currentItemLi.siblings().removeClass('disabled').prop('disabled', false);
        currentItemLi.siblings().children().removeClass('disabled').prop('disabled', false);
        $("#todos").sortable("enable");
        enableSortableWithCookie();
        todoCookieSet();

    }









});
